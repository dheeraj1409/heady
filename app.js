const express=require('express');
const app=express();
var cors = require('cors');
const users = require('./routes/api.routes');
const bodyParser = require('body-parser');

app.use(cors());
app.use(bodyParser.json());
app.use('/',users);

app.listen(7005,()=>{
console.log('Listening to port 7005');
});