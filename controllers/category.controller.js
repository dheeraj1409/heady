const mongoose = require('mongoose');
const dbConfig = require('../dbConfig');
const categoryModel = require('../models/category.model');



module.exports = {

    // add new category in category collection
    async createCategory(req, res) {

        // decalre incoming request paramter.
        const categoryName = req.body.categoryName ? req.body.categoryName : '';

        try {
            // create new document for new category
            var category = new categoryModel({
                categoryName: categoryName
            })

            //insert the new document into the category collection
            category.save(function (err) {
                if (!err) {
                    return res.status(200).json({
                        status: 200,
                        success: true,
                        message: 'Category Added Successfully',
                    });
                }
                else if(err.code==11000){
                    return res.status(200).json({
                        status: 200,
                        success: true,
                        message: 'Category Already Exist',
                    });
                }
                else {
                    return res.status(400).json({
                        message: 'Error creating Category',
                        success: false,
                        error: err,
                    });
                }
            })


        } catch (err) {

            return res.status(500).json({
                message: 'Error',
                success: false,
                error: err,
            });
        }
    },


    // create sub category for specific category
    async createSubCategory(req, res) {

        // decalre incoming request paramter.
        const subCategoryName = req.body.subCategoryName ? req.body.subCategoryName : '';
        const categoryId = req.body.categoryId ? req.body.categoryId : '';

        try {

            // create new array for new sub category
            let subCategoryModel = {
                subCategory: {
                    subCategoryName: subCategoryName
                }
            }

            // add the sub category against respective category.
            categoryModel.findByIdAndUpdate(categoryId,{ $addToSet: subCategoryModel }, function (err, result) {
                if (!err) {
                    return res.status(200).json({
                        status: 200,
                        success: true,
                        message: 'Sub Category Creating Successfully',
                    });
                }
                else if(err.code==11000){
                    return res.status(200).json({
                        status: 200,
                        success: true,
                        message: 'Sub Category Already Exist',
                    });
                }
                else {
                    return res.status(400).json({
                        message: 'Error creating Sub Category',
                        success: false,
                        error: err,
                    });
                }
            })


        } catch (err) {

            return res.status(500).json({
                message: 'Error',
                success: false,
                error: err,
            });
        }
    },

    // create sub sub category for specific category
    async createSubSubCategory(req, res) {

        // decalre incoming request paramter.
        const subSubCategoryName = req.body.subSubCategoryName ? req.body.subSubCategoryName : '';
        const categoryId = req.body.categoryId ? req.body.categoryId : '';
        const subCategoryId = req.body.subCategoryId ? req.body.subCategoryId : '';

        try {
            // create new array for new sub sub category
            let subSubCategoryModel = {
               
                    subSubCategoryName: subSubCategoryName
                
            }

            // add the sub sub category against respective category and sub category.
            categoryModel.findOneAndUpdate({_id:categoryId,subCategory:{$elemMatch:{'_id':subCategoryId}}},{$push:{'subCategory.$.subSubCategory':subSubCategoryModel}}, function (err, result) {
                if (!err) {
                    return res.status(200).json({
                        status: 200,
                        success: true,
                        message: 'Sub Sub Category Creating Successfully',
                    });
                }
                else if(err.code==11000){
                    return res.status(200).json({
                        status: 200,
                        success: true,
                        message: 'Sub Category Already Exist',
                    });
                }
                else {
                    return res.status(400).json({
                        message: 'Error creating Sub Category',
                        success: false,
                        error: err,
                    });
                }
            })


        } catch (err) {

            return res.status(500).json({
                message: 'Error',
                success: false,
                error: err,
            });
        }
    },



    
    // view all category
    async viewCategories(req, res) {
        
        try {

            // get all category from the category collection along with sub category and sub sub category.
            categoryModel.find(function(err,result){
                if(!err){
                    return res.status(200).json({
                        status: 200,
                        success: true,
                        result:result
                    });
                }
                else{
                    return res.status(400).json({
                        status: false,
                        message: 'Error Viewing category',
                        success: false,
                        error: err,
                    });
                }
            })            

        } catch (err) {
            
            return res.status(500).json({
                status: false,
                message: 'Error Viewing events',
                success: false,
                error: errors,
            });
        }

    },

}