const mongoose = require('mongoose');
const dbConfig = require('../dbConfig');
const categoryModel = require('../models/category.model');
const productModel = require('../models/product.model');


module.exports={

    // add product Api function
    async addProduct(req, res) {

        // decalre incoming request paramter.
        const productName = req.body.productName ? req.body.productName : '';
        const categoryId = req.body.categoryId ? req.body.categoryId : 0;
        const productPrice = req.body.productPrice ? req.body.productPrice : 0;
        
        try {
            //search into product collection weather the incoming product name exist in collection or not
            productModel.find({productName:productName},function(err,result){
                
                //check if incoming product name is already present into product colletion
                if(result.length>0){
                    
                    //bind the request parameter into category array
                    let categoryId={
                        categoryId:{
                            categoryId:req.body.categoryId,
                            productPrice: req.body.productPrice
                        }
                    }

                    // add the incoming request category and price into existing product name entry. 
                    productModel.findOneAndUpdate({productName:productName},{$addToSet:categoryId},function(err1,result1){
                        if(!err1){
                        return res.status(200).json({
                            status: 200,
                            success: true,
                            message: 'Product Added Successfully',
                        });
                    }
                    else{
                        return res.status(400).json({
                            message: 'Error creating Product',
                            success: false,
                            error: err
                        });
                    }
                    })
                }
                else{
                    // create new document for new product name
                    var product = new productModel({
                        productName: productName,
                        categoryId:{
                            categoryId:categoryId,
                            productPrice: productPrice
                        }
                    })

                    //insert the new document into the product collection
                    product.save(function (err) {
                        if (!err) {
                            return res.status(200).json({
                                status: 200,
                                success: true,
                                message: 'Product Added Successfully',
                            });
                        }
                        else if(err.code==11000){
                            return res.status(200).json({
                                status: 200,
                                success: true,
                                message: 'Product Already Exist',
                            });
                        }
                        else {
                            return res.status(400).json({
                                message: 'Error creating Product',
                                success: false,
                                error: err,
                            });
                        }
                    })
                }

            })

        } catch (err) {
            console.log(err)
            return res.status(500).json({
                message: 'Error',
                success: false,
                error: err,
            });
        }
    },


    // edit product Api function
    async editProduct(req, res) {

        // decalre incoming request paramter.
        const productName = req.body.productName ? req.body.productName : '';
        const productId = req.body.productId ? req.body.productId : 0;
        const categoryId = req.body.categoryId ? req.body.categoryId : 0;
        const productPrice = req.body.productPrice ? req.body.productPrice : 0;
        
        try {

            // update the product name or product price base on product _id and categoryId
            productModel.findOneAndUpdate({_id:productId,categoryId:{$elemMatch:{'categoryId':categoryId}}},{ $set: {productName:productName, 'categoryId.$.productPrice': productPrice}},function(err,result){

                if(!err){
                    return res.status(200).json({
                        status: 200,
                        success: true,
                        message: 'Product Editeds Successfully',
                    });
                }
                else if(err.code==11000){
                    return res.status(200).json({
                        status: 200,
                        success: true,
                        message: 'Product Already Exist',
                    });
                }
                else{
                    return res.status(500).json({
                        message: 'Error',
                        success: false,
                        error: err,
                    });
                }


            })

        } catch (err) {
            console.log(err)
            return res.status(500).json({
                message: 'Error',
                success: false,
                error: err,
            });
        }
    },

    // view all product with their respective category
    async viewAllProduct(req, res) {

        try {

            // get all product from the product collection.
            productModel.aggregate([{$unwind:"$categoryId"},{
                $lookup:{
                    from: "categories",
                    localField: "categoryId.categoryId",
                    foreignField: "_id",
                    as: "categoryData"
                }
            },
            {
                $project:{
                    "_id":1,
                    "productName":1,
                    "categoryId.categoryId":1,
                    "categoryId.productPrice":1,
                    "categoryData.categoryName":1

                }
            }],function(err,result){
                console.log('result',err)
                if(!err){
                    return res.status(200).json({
                        status: 200,
                        success: true,
                        result: result
                    });
                }
                else{
                    return res.status(500).json({
                        message: 'Error',
                        success: false,
                        error: err,
                    });
                }


            })

        } catch (err) {
            console.log(err)
            return res.status(500).json({
                message: 'Error',
                success: false,
                error: err,
            });
        }
    },


    // get all product based on specific category
    async viewProductByCategory(req, res) {

        var categoryId=req.body.categoryId              // get categoryId
        var id = mongoose.Types.ObjectId(categoryId);   // convert that categoryID into objectID
        
        try {
            // get all product from the product collection based on the incoming category id.
            productModel.aggregate([{$unwind:"$categoryId"},{
                $lookup:{
                    from: "categories",
                    localField: "categoryId.categoryId",
                    foreignField: "_id",
                    as: "categoryData"
                }
            },
            {
                $project:{
                    "_id":1,
                    "productName":1,
                    "categoryId":1,
                    "categoryData.categoryName":1

                }
            },
            {$match:{"categoryId.categoryId":id}}],function(err,result){
                console.log('result',result)
                if(!err){
                    return res.status(200).json({
                        status: 200,
                        success: true,
                        result: result
                    });
                }
                else{
                    return res.status(500).json({
                        message: 'Error',
                        success: false,
                        error: err,
                    });
                }


            })

        } catch (err) {
            console.log(err)
            return res.status(500).json({
                message: 'Error',
                success: false,
                error: err,
            });
        }
    },

}