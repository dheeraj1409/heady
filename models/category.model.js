const mongoose =require('mongoose');

let categoryMaster=mongoose.Schema({
    categoryName:{
        type:String,
        unique :true,
        require:[true,'Is required']
    },
    subCategory:[{
        subCategoryName:{
            type:String,
            unique :true
        },
        subSubCategory:[{
            subSubCategoryName:{
                type:String,
                unique :true
            }
        }]
    }]



})

const categories = mongoose.model('category', categoryMaster);
module.exports = categories;