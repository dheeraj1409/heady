const mongoose =require('mongoose');

let productMaster=mongoose.Schema({
    productName:{
        type:String,
        unique :true,
        require:[true,'Is required']
    },
    categoryId:[{
        categoryId:{type:mongoose.Schema.ObjectId,unique :true},
        productPrice:Number
    }]


})

const products = mongoose.model('product', productMaster);
module.exports = products;