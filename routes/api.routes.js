const express = require('express');
const router = express.Router();
const categoryController= require('../controllers/category.controller.js');
const productController= require('../controllers/product.controller.js');

router.post('/createCategory', categoryController.createCategory);

router.post('/createSubCategory', categoryController.createSubCategory);

router.post('/createSubSubCategory', categoryController.createSubSubCategory);

router.post('/viewCategories', categoryController.viewCategories);

router.post('/addProduct', productController.addProduct);

router.post('/editProduct', productController.editProduct);

router.post('/viewAllProduct', productController.viewAllProduct);

router.post('/viewProductByCategory', productController.viewProductByCategory);

module.exports = router;